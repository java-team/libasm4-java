#!/bin/sh

set -eu

VERSION=$2
TAR=../libasm4-java_$VERSION.orig.tar.xz
DIR=asm-$VERSION
TAG=$(echo "ASM_$VERSION" | sed -re's,\.,_,g')

rm $3
svn export svn://svn.forge.objectweb.org/svnroot/asm/tags/${TAG}/ $DIR
XZ_OPT=--best tar -c -J -f $TAR --exclude 'config/*.jar' $DIR
rm -rf $DIR
